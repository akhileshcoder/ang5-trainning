import { Component } from '@angular/core';

import { Http,Headers } from "@angular/http";

@Component({
  selector: 'profile',
  templateUrl: './profile.html',
  styleUrls: ['./profile.css']
})
export class profile {

  prof=null;

  constructor(private http:Http){
    this.getDataFromServer();
  }
  getDataFromServer(){
    let hdr = new Headers();
    this.http.get("http://localhost:5009/profile-data?user=akhilesh",{
      headers:hdr
    }).subscribe(data=>{
      this.prof = data.json();
    })
  }
  giveKeys(i){
    return Object.keys(i);
  }
}



