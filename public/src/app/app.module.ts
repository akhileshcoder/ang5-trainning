import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';


import { AppComponent } from './app.component';
import { password } from '../password/password';
import { registration } from '../registration/registration';
import {login} from '../login/login';
import { profile } from '../profile/profile';
import { dashboard } from '../dashboard/dashboard';
import { logout } from '../logout/logout';

import { AppRoutingModule }     from './myRoutes';


@NgModule({
  declarations: [
    AppComponent, password, registration, login, profile, dashboard, logout
  ],
  imports: [
    BrowserModule, FormsModule, AppRoutingModule, HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
